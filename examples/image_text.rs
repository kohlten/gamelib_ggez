use gamelib_ggez::asset_database::AssetDatabase;
use gamelib_ggez::{AppBuilder, DrawCommandManager, Event};
use gamelib_ggez::image_text::{GlyphTable, ImageText, ImageTextValue};
use gamelib_ggez::scene::{Scene, SceneEvent, SceneTrait};
use gamelib_ggez::ggez::{ContextBuilder, event, Context, GameResult};
use gamelib_ggez::ggez::event::KeyCode;
use gamelib_ggez::ggez::graphics::Color;
use gamelib_ggez::glam::{Vec2, Vec4};
use imgui::{Ui, Style};

pub struct MainWindow {
    image_text: ImageText,
    size: i32,
}

impl MainWindow {
    pub fn new(ctx: &mut Context) -> Self {
        let glyphs = GlyphTable::from_config("z.toml").unwrap();
        let mut image_text = ImageText::new(glyphs, ctx).unwrap();
        image_text.add_value(ImageTextValue::new("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\n~`!@#$%^&*()-_+=[{}]|:;\"'<,>.?/1234567890", Vec2::new(0.0, 0.0), Color::new(1.0, 0.0, 0.0, 1.0), 100));
        Self { image_text, size: 100 }
    }
}

impl SceneTrait for MainWindow {
    fn update(&mut self, _ctx: &mut Context, _asset_database: &mut AssetDatabase, _dt: f64) -> SceneEvent {
        let mut image_text = self.image_text.get_value(0).unwrap();
        image_text.set_font_size(self.size);
        self.image_text.set_value(0, image_text);
        SceneEvent::None
    }

    fn render(&mut self, _ctx: &mut Context, draw_command_manager: &mut DrawCommandManager, _asset_database: &AssetDatabase) {
        self.image_text.draw(draw_command_manager);
    }

    fn render_ui(&mut self, _ctx: &mut Context, ui: &Ui, _style: Style, _asset_database: &AssetDatabase) {
    }

    fn event(&mut self, _ctx: &mut Context, _asset_database: &AssetDatabase, event: &Event) -> SceneEvent {
        match event {
            Event::KeyEvent(args) => {
                if args.press {
                    if args.key_code == KeyCode::Plus {
                        self.size += 5;
                    } else if args.key_code == KeyCode::Minus {
                        self.size -= 5;
                    }
                }
            }
            _ => {}
        }
        SceneEvent::None
    }

    fn reset(&mut self) {
    }
}

fn main() -> GameResult {
    let (mut ctx, event_loop) = ContextBuilder::new("image_text", "kohlten").build().unwrap();
    let mut app = AppBuilder::default().clear_color(Color::new(1.0, 1.0, 1.0, 1.0)).build(&mut ctx);
    app.scene_manager.add_scene("main_window", Scene::new(Box::new(MainWindow::new(&mut ctx)))).unwrap();
    event::run(ctx, event_loop, app)
}