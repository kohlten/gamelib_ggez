use ggez::event::{KeyCode, KeyMods, GamepadId, Axis, MouseButton, Button};

#[derive(Copy, Clone, PartialEq)]
pub struct MouseButtonArgs {
    pub button: MouseButton,
    pub x: f32,
    pub y: f32,
    pub press: bool,
}

impl MouseButtonArgs {
    pub(crate) fn new(button: MouseButton, x: f32, y: f32, press: bool) -> Self {
        Self { button, x, y, press }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct MouseMotionArgs {
    pub x: f32,
    pub y: f32,
    pub dx: f32,
    pub dy: f32,
}

impl MouseMotionArgs {
    pub(crate) fn new(x: f32, y: f32, dx: f32, dy: f32) -> Self {
        Self { x, y, dx, dy }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct MouseWheelArgs {
    pub x: f32,
    pub y: f32,
}

impl MouseWheelArgs {
    pub(crate) fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct KeyEventArgs {
    pub key_code: KeyCode,
    pub key_mods: KeyMods,
    pub repeat: bool,
    pub press: bool,
}

impl KeyEventArgs {
    pub(crate) fn new(key_code: KeyCode, key_mods: KeyMods, repeat: bool, press: bool) -> Self {
        Self { key_code, key_mods, repeat, press }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct GamepadButtonArgs {
    pub button: Button,
    pub id: GamepadId,
    pub press: bool,
}

impl GamepadButtonArgs {
    pub(crate) fn new(button: Button, id: GamepadId, press: bool) -> Self {
        Self { button, id, press }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct GamepadAxisArgs {
    pub axis: Axis,
    pub id: GamepadId,
    pub value: f32,
}

impl GamepadAxisArgs {
    pub(crate) fn new(axis: Axis, id: GamepadId, value: f32) -> Self {
        Self { axis, id, value }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct ResizeArgs {
    pub w: f32,
    pub h: f32,
}

impl ResizeArgs {
    pub(crate) fn new(w: f32, h: f32) -> Self {
        Self { w, h }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub enum Event {
    MouseButton(MouseButtonArgs),
    MouseMotion(MouseMotionArgs),
    MouseEntry(bool),
    MouseWheel(MouseWheelArgs),
    KeyEvent(KeyEventArgs),
    TextInput(char),
    GamepadButton(GamepadButtonArgs),
    GamepadAxis(GamepadAxisArgs),
    Focus(bool),
    Quit,
    Resize(ResizeArgs),
}