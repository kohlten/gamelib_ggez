use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Animation already has {value:?} stored as an animation")]
    AnimationAlreadyHasAnimation {
        value: String,
    },
    #[error("Animation {value:?} has not been added")]
    AnimationNotFound {
        value: String
    },

    #[error("Failed to create window with error: {reason:?}")]
    WindowCreation {
        reason: String
    },

    #[error("Global {name:?} already exists")]
    GlobalAlreadyExists {
        name: String
    },
    #[error("Global {name:?} doesn't exist")]
    GlobalDoesntExist {
        name: String
    },
    #[error("SPRITE_SHEET_LOAD: {error:?}")]
    SpriteSheetError {
        error: String,
    },
    #[error("Failed to load font {path:?}")]
    FontLoadError {
        path: String
    }
}