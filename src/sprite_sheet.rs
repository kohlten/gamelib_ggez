use ggez::graphics::spritebatch::SpriteBatch;
use ggez::graphics::{Image, DrawParam, Rect};
use ggez::Context;
use anyhow::Result;
use crate::draw_command::{DrawCommand, DrawCommandManager};
use std::fs::File;
use std::io::{Write, Read};
use serde_derive::{Serialize, Deserialize};

#[derive(Deserialize, Serialize)]
pub struct SpriteSheetConfig {
    image_name: String,
    sections: Vec<Rect>
}

pub struct SpriteSheet {
    image_name: String,
    batch: SpriteBatch,
    sections: Vec<Rect>,
}

impl SpriteSheet {
    pub fn new(image_name: &str, sections: Vec<Rect>, context: &mut Context) -> Result<Self> {
        let image = Image::new(context, image_name)?;
        Ok(Self { image_name: image_name.to_string(), batch: SpriteBatch::new(image), sections })
    }

    pub fn from_config(name: &str, context: &mut Context) -> Result<Self> {
        let mut data = Vec::new();
        File::open(name)?.read_to_end(&mut data)?;
        let config: SpriteSheetConfig = toml::from_slice(&data)?;
        let img = Image::new(context, config.image_name.clone())?;
        Ok(Self {image_name: config.image_name, batch: SpriteBatch::new(img), sections: config.sections})
    }

    pub fn to_config(&self, name: &str) -> Result<()> {
        let config = SpriteSheetConfig { image_name: self.image_name.clone(), sections: self.sections.clone() };
        let toml = toml::to_string(&config)?;
        match File::create(name)?.write_all(toml.as_bytes()) {
            Ok(_) => { Ok(()) }
            Err(err) => { Err(anyhow::Error::from(err)) }
        }
    }

    /// Adds a new section to be drawn to SpriteBatch. idx is the index of the section of the image you wish to draw.
    pub fn add_draw_section(&mut self, idx: usize, mut param: DrawParam) {
        if idx >= self.sections.len() {
            return; // @TODO Make error
        }
        param.src = self.sections[idx];
        self.batch.add(param);
    }

    pub fn draw(&mut self, draw_command_manager: &mut DrawCommandManager, param: DrawParam) {
        draw_command_manager.push(DrawCommand::SpriteBatch((self.batch.clone(), param)));
        self.batch.clear();
    }

    pub fn len(&self) -> usize {
        self.sections.len()
    }
}