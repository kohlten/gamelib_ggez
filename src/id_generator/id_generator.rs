use num::{Integer, NumCast, Zero};
use std::ops::AddAssign;

/// Id generator to get unique ids
pub struct IDGenerator<T> {
    last_entity: T,
}

impl<T: Integer + Copy + AddAssign + Zero + NumCast> IDGenerator<T> {
    /// Creates a new IDGenerator
    pub(crate) fn new() -> IDGenerator<T> {
        IDGenerator { last_entity: num::cast(0).unwrap() }
    }

    /// Returns a new unique id
    pub(crate) fn new_id(&mut self) -> T {
        let id = self.last_entity;
        self.last_entity += num::cast(1).unwrap();
        id
    }
}