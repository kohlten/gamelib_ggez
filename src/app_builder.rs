use ggez::graphics::Color;
use crate::App;
use ggez::Context;

pub(crate) struct AppConfig {
    clear_color: Color,
}

impl AppConfig {
    pub(crate) fn get_clear_color(&self) -> Color {
        self.clear_color
    }
}

impl Default for AppConfig {
    fn default() -> Self {
        Self { clear_color: Color::new(0.0, 0.0, 0.0, 1.0) }
    }
}

#[derive(Default)]
pub struct AppBuilder {
    config: AppConfig
}

impl AppBuilder {
    pub fn clear_color(mut self, clear_color: Color) -> Self {
        self.config.clear_color = clear_color;
        self
    }

    pub fn build(self, ctx: &mut Context) -> App {
        App::new(ctx, self.config)
    }
}