use std::time::Duration;
use std::collections::HashMap;
use crate::error::{Error};
use crate::error::Error::{AnimationNotFound, AnimationAlreadyHasAnimation};
use crate::timer::Timer;
use crate::draw_command::{DrawCommand, DrawCommandManager};
use ggez::graphics::{Image, DrawParam};
use crate::animation_image_lookup::{AnimationLookup, AnimationImageLookup};

/// Play the current animation forever
pub const ANIMATION_PLAY_INFINITELY: i64 = -1;

// @TODO Look into seeing if wait times need to be specific to the image or a set number
// @TODO Make this accept a sprite sheet, or a list of images (Problem with this is i'm not sure how to draw a SpriteSheet or an image using ggez.
// @TODO They require seperate draw calls. If dyn Drawable would work, this problem would be fixed.
// @TODO For now, just going to do a list of Images and do the SpriteBatch optimization later.)
/// Animation node in the Animation. Stores the state of the animation.
pub struct AnimationNode<T>
    where T: AnimationLookup {
    textures: T,
    current_image: u32,
    times_played: u32,
    max_times_to_play: i64,
    timer: Timer,
    playing: bool,
}

impl<T> AnimationNode<T>
    where T: AnimationLookup {
    /// Creates a new node in the Animation
    pub fn new(textures: T, wait_time: Duration, max_times_to_play: i64) -> AnimationNode<T> {
        AnimationNode {
            textures,
            current_image: 0,
            times_played: 0,
            max_times_to_play,
            timer: Timer::new(wait_time),
            playing: false,
        }
    }

    /// Sets the current image in this node
    pub fn set_current_image(&mut self, current_image: u32) { self.current_image = current_image; }

    /// Sets the wait time in this node
    pub fn set_wait_time(&mut self, wait_time: Duration) { self.timer.set_target_time(wait_time); }

    /// Sets the max times to play this node
    pub fn set_max_times_to_play(&mut self, max_times_to_play: i64) { self.max_times_to_play = max_times_to_play; }

    /// Sets if we are playing the animation
    pub fn set_playing(&mut self, playing: bool) {
        self.playing = playing;
        if playing {
            self.timer.start();
            self.times_played = 0;
        }
    }

    /// Get if we are playing the animation
    pub fn get_playing(&self) -> bool { return self.playing; }

    /// Update the node if it's playing
    pub fn update(&mut self) {
        if self.playing {
            if self.timer.is_completed() {
                if self.current_image >= (self.textures.len() - 1) as u32 {
                    self.current_image = 0;
                    self.times_played += 1;
                }
                self.current_image += 1;
                self.timer.start();
            }
            if self.times_played as i64 > self.max_times_to_play && self.max_times_to_play <= ANIMATION_PLAY_INFINITELY {
                self.timer.stop();
                self.playing = false;
            }
        }
    }

    // @TODO Should draw here if not playing?
    /// Draw the node if we are playing
    pub fn draw(&mut self, params: DrawParam, draw_command_manager: &mut DrawCommandManager) {
        if self.playing {
            self.textures.add_frame(self.current_image as usize);
            self.textures.draw(params, draw_command_manager);
        }
    }
}

// @TODO Would love to add animation_image_lookup here, but we then don't have information for the AnimationNode.
// @TODO We need a way of getting the data for the animation node and the images.
// @TODO Might be possible to combine the two or change each node to accept a AnimationLookup.
/// Stores all the animations and the current state. The animation can have multiple states, useful for a sprite sheet with multiple animations.
pub struct Animation<T>
    where T: AnimationLookup {
    states: HashMap<String, AnimationNode<T>>,
    current_state: String,
}

impl<T: AnimationLookup> Animation<T> {
    /// Create a new animation.
    pub fn new() -> Animation<T> {
        Animation { states: HashMap::new(), current_state: "".parse().unwrap() }
    }

    /// Adds a new node to the Animation. Will return AnimationAlreadyHasAnimation if the name already exists.
    pub fn add_state(&mut self, name: String, node: AnimationNode<T>) -> Result<(), Error> {
        if self.states.contains_key(&name) {
            return Err(AnimationAlreadyHasAnimation { value: name });
        }
        self.states.insert(name, node);
        Ok(())
    }

    /// Removes the name from the Animation. If the state isn't
    pub fn remove_state(&mut self, name: String) -> Result<(), Error> {
        if !self.states.contains_key(&name) {
            return Err(AnimationNotFound { value: name });
        }
        self.states.remove(&name);
        Ok(())
    }

    /// Sets the state of the Animation. Will return AnimationNotFound if the name isn't found.
    pub fn set_state(&mut self, name: String) -> Result<(), Error> {
        if !self.states.contains_key(&name) {
            return Err(AnimationNotFound { value: name });
        }
        self.current_state = name;
        Ok(())
    }

    pub fn draw(&mut self, param: DrawParam, draw_command_manager: &mut DrawCommandManager) {
        if self.current_state != "" {
            let node = self.states.get_mut(&self.current_state).unwrap();
            node.draw(param, draw_command_manager);
        }
    }

    pub fn update(&mut self) {
        if self.current_state != "" {
            let node = &mut self.states.get_mut(&self.current_state).unwrap();
            node.update();
        }
    }
}
