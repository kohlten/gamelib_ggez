use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Scene {name:?} doesn't exist")]
    SceneDoesntExist {
        name: String
    },
    #[error("Scene {name:?} already exists")]
    SceneAlreadyExists {
        name: String
    }
}