use crate::asset_database::AssetDatabase;
use crate::event::Event;
use crate::draw_command::DrawCommandManager;
use ggez;
use imgui::Style;
use crate::ggez::Context;
use crate::imgui::Ui;

pub trait SceneTrait {
    fn update(&mut self, _ctx: &mut Context, _asset_database: &mut AssetDatabase, _dt: f64) -> SceneEvent { SceneEvent::None }
    fn render(&mut self, _ctx: &mut Context, _draw_command_manager: &mut DrawCommandManager, _asset_database: &AssetDatabase) {}
    fn render_ui(&mut self, _ctx: &mut Context, _ui: &Ui, _imgui_style: Style, _asset_database: &AssetDatabase) {}
    fn event(&mut self, _ctx: &mut Context, _asset_database: &AssetDatabase, _event: &Event) -> SceneEvent { SceneEvent::None }
    fn reset(&mut self) {}
}

/// A Scene in the scene manager with event callbacks
pub struct Scene {
    scene_trait: Box<dyn SceneTrait>,
}

/// Scene event. Allows the scene to change scenes in the SceneManager
pub enum SceneEvent {
    None,
    ChangeScene(String),
}

impl Scene {
    /// Creates a new scene with event callbacks
    pub fn new(scene_trait: Box<dyn SceneTrait>) -> Self {
        Self {
            scene_trait
        }
    }

    /// Updates the scene using the trait func
    pub fn update(&mut self, ctx: &mut Context, asset_database: &mut AssetDatabase, dt: f64) -> SceneEvent {
        self.scene_trait.update(ctx, asset_database, dt)
    }

    /// Renders the scene using the trait func
    pub fn render(&mut self, ctx: &mut Context, draw_command_manager: &mut DrawCommandManager, asset_database: &AssetDatabase) {
        self.scene_trait.render(ctx, draw_command_manager, asset_database);
    }

    // Renders the ui
    pub fn render_ui(&mut self, ctx: &mut Context, ui: &Ui, imgui_style: Style, asset_database: &AssetDatabase) {
        self.scene_trait.render_ui(ctx, ui, imgui_style, asset_database);
    }

    /// Calls the event callback
    pub fn event(&mut self, ctx: &mut Context, asset_database: &AssetDatabase, event: &Event) -> SceneEvent {
        self.scene_trait.event(ctx, asset_database, event)
    }

    /// Resets the scene using the trait func
    pub fn reset(&mut self) {
        self.scene_trait.reset();
    }
}