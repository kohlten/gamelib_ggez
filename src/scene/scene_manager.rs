use std::collections::HashMap;

use ggez::Context;

use crate::asset_database::AssetDatabase;
use crate::draw_command::DrawCommandManager;
use crate::event::Event;
use crate::imgui::{Style, Ui};
use crate::scene::{Scene, SceneEvent};
use crate::scene::error::Error;

/// Scene manager
pub struct SceneManager {
    scenes: HashMap<String, Scene>,
    current_scene: String,
}

impl SceneManager {
    /// Creates a new SceneManager
    pub fn new() -> SceneManager {
        SceneManager { scenes: HashMap::new(), current_scene: "".to_string() }
    }

    /// Adds a scene to this manager under name. Returns SceneAlreadyExists if it already has a scene under that name.
    /// Will also set the current scene to that scene if it's the first scene added.
    pub fn add_scene(&mut self, name: &str, scene: Scene) -> Result<(), Error> {
        if self.scenes.contains_key(name) {
            return Err(Error::SceneAlreadyExists { name: name.to_string() });
        }
        self.scenes.insert(name.to_string(), scene);
        if self.current_scene == "" {
            self.current_scene = name.to_string();
        }
        Ok(())
    }

    /// Removes the scene from this manager. If the scene isn't in the manager, returns SceneDoesntExist.
    pub fn remove_scene(&mut self, name: &str) -> Result<(), Error> {
        if !self.scenes.contains_key(name) {
            return Err(Error::SceneDoesntExist { name: name.to_string() });
        }
        self.scenes.remove(name);
        Ok(())
    }

    /// Sets the current scene. If the scene isn't in the manager, returns SceneDoesntExist.
    /// Also resets the current scene if there is one
    pub fn set_current_scene(&mut self, name: &str) -> Result<(), Error> {
        if !self.scenes.contains_key(name) {
            return Err(Error::SceneDoesntExist { name: name.to_string() });
        }
        if self.current_scene != "" {
            self.scenes.get_mut(&self.current_scene).unwrap().reset();
        }
        self.current_scene = name.to_string();
        Ok(())
    }

    /// Updates the current scene
    pub fn update(&mut self, ctx: &mut Context, asset_database: &mut AssetDatabase, dt: f64) {
        if self.current_scene == "" {
            return;
        }
        let scene = self.scenes.get_mut(&self.current_scene).unwrap();
        let event = scene.update(ctx, asset_database, dt);
        match event {
            SceneEvent::ChangeScene(to) => {
                self.set_current_scene(&to).expect("ERROR: Failed to change to nonexistent scene");
            }
            _ => {}
        }
    }

    pub fn render_ui(&mut self, ggez_ctx: &mut ggez::Context, ui: &Ui, imgui_style: Style, asset_database: &AssetDatabase) {
        if self.current_scene == "" {
            return;
        }
        let scene = self.scenes.get_mut(&self.current_scene).unwrap();
        scene.render_ui(ggez_ctx, ui, imgui_style, asset_database);
    }

    /// Renders the current scene
    pub fn render(&mut self, ctx: &mut Context, draw_command_manager: &mut DrawCommandManager, asset_database: &AssetDatabase) {
        if self.current_scene == "" {
            return;
        }
        let scene = self.scenes.get_mut(&self.current_scene).unwrap();
        scene.render(ctx, draw_command_manager, asset_database);
    }

    /// Resets the current scene
    pub fn reset(&mut self) {
        if self.current_scene == "" {
            return;
        }
        let scene = self.scenes.get_mut(&self.current_scene).unwrap();
        scene.reset();
    }

    /// Returns the current scene name
    pub fn get_state(&self) -> String {
        self.current_scene.clone()
    }

    pub(crate) fn event(&mut self, ctx: &mut Context, asset_database: &AssetDatabase, event: &Event) {
        if self.current_scene != "" {
            let scene = self.scenes.get_mut(&self.current_scene).unwrap();
            let event = scene.event(ctx, asset_database, event);
            match event {
                SceneEvent::ChangeScene(to) => {
                    self.set_current_scene(&to).expect("ERROR: Failed to change to nonexistent scene");
                }
                _ => {}
            }
        }
    }
}