use ggez::event::{EventHandler, MouseButton, KeyMods, Button, GamepadId, Axis, KeyCode};
use ggez::{GameError, Context, GameResult, graphics};
use event_manager::EventManager;
use crate::event::{Event, MouseButtonArgs, MouseMotionArgs, MouseWheelArgs, KeyEventArgs, GamepadButtonArgs, GamepadAxisArgs, ResizeArgs};
use crate::draw_command::{DrawCommand, DrawCommandManager};
use crate::scene::SceneManager;
use crate::asset_database::AssetDatabase;
use crate::app_builder::AppConfig;
use std::cell::RefCell;
use std::time::Instant;

use gfx_core::{handle::RenderTargetView, memory::Typed};
use gfx_device_gl;
use imgui::*;
use imgui_gfx_renderer::*;

#[derive(Copy, Clone, PartialEq, Debug, Default)]
struct MouseState {
    pos: (i32, i32),
    /// mouse buttons: (left, right, middle)
    pressed: (bool, bool, bool),
    wheel: f32,
    wheel_h: f32,
}

pub struct App {
    pub scene_manager: SceneManager,
    pub asset_database: AssetDatabase,
    pub imgui: imgui::Context,
    pub renderer: Renderer<gfx_core::format::Rgba8, gfx_device_gl::Resources>,
    last_frame: Instant,
    events: EventManager<Event>,
    draw_command_manager: DrawCommandManager,
    config: AppConfig,
    mouse_state: MouseState,
}

impl App {
    pub(crate) fn new(ctx: &mut Context, config: AppConfig) -> Self {
        let mut imgui = imgui::Context::create();
        let (factory, gfx_device, _, _, _) = graphics::gfx_objects(ctx);
        let shaders = {
            let version = gfx_device.get_info().shading_language;
            if version.is_embedded {
                if version.major >= 3 {
                    Shaders::GlSlEs300
                } else {
                    Shaders::GlSlEs100
                }
            } else if version.major >= 4 {
                Shaders::GlSl400
            } else if version.major >= 3 {
                Shaders::GlSl130
            } else {
                Shaders::GlSl110
            }
        };
        let renderer = Renderer::init(&mut imgui, &mut *factory, shaders).unwrap();
        Self {
            imgui,
            renderer,
            last_frame: Instant::now(),
            events: EventManager::new(),
            draw_command_manager: DrawCommandManager::new(),
            scene_manager: SceneManager::new(),
            asset_database: AssetDatabase::new(),
            config,
            mouse_state: Default::default()
        }
    }

    fn imgui_io_init(&mut self) {
        let io = self.imgui.io_mut();
        io[Key::Tab] = KeyCode::Tab as _;
        io[Key::LeftArrow] = KeyCode::Left as _;
        io[Key::RightArrow] = KeyCode::Right as _;
        io[Key::UpArrow] = KeyCode::Up as _;
        io[Key::DownArrow] = KeyCode::Down as _;
        io[Key::PageUp] = KeyCode::PageUp as _;
        io[Key::PageDown] = KeyCode::PageDown as _;
        io[Key::Home] = KeyCode::Home as _;
        io[Key::End] = KeyCode::End as _;
        io[Key::Insert] = KeyCode::Insert as _;
        io[Key::Delete] = KeyCode::Delete as _;
        io[Key::Backspace] = KeyCode::Back as _;
        io[Key::Space] = KeyCode::Space as _;
        io[Key::Enter] = KeyCode::Return as _;
        io[Key::Escape] = KeyCode::Escape as _;
        io[Key::KeyPadEnter] = KeyCode::NumpadEnter as _;
        io[Key::A] = KeyCode::A as _;
        io[Key::C] = KeyCode::C as _;
        io[Key::V] = KeyCode::V as _;
        io[Key::X] = KeyCode::X as _;
        io[Key::Y] = KeyCode::Y as _;
        io[Key::Z] = KeyCode::Z as _;
    }
}

impl EventHandler<GameError> for App {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        while let Some(event) = self.events.pop() {
            self.scene_manager.event(ctx, &self.asset_database, &event);
        }
        self.scene_manager.update(ctx, &mut self.asset_database, ggez::timer::delta(ctx).as_secs_f64());
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, self.config.get_clear_color());
        self.scene_manager.render(ctx, &mut self.draw_command_manager, &self.asset_database);
        {
            self.imgui.io_mut().mouse_pos = [self.mouse_state.pos.0 as f32, self.mouse_state.pos.1 as f32];
            self.imgui.io_mut().mouse_down = [
                self.mouse_state.pressed.0,
                self.mouse_state.pressed.1,
                self.mouse_state.pressed.2,
                false,
                false,
            ];
            self.imgui.io_mut().mouse_wheel = self.mouse_state.wheel;
            self.mouse_state.wheel = 0.0;
            self.imgui.io_mut().mouse_wheel_h = self.mouse_state.wheel_h;
            self.mouse_state.wheel_h = 0.0;
            let hidpi_factor = 1.0;
            let now = Instant::now();
            let delta = now - self.last_frame;
            let delta_s = delta.as_secs() as f32 + delta.subsec_nanos() as f32 / 1_000_000_000.0;
            self.last_frame = now;
            let (draw_width, draw_height) = graphics::drawable_size(ctx);
            self.imgui.io_mut().display_size = [draw_width, draw_height];
            self.imgui.io_mut().display_framebuffer_scale = [hidpi_factor, hidpi_factor];
            self.imgui.io_mut().delta_time = delta_s;
        }
        {
            let style = self.imgui.style().clone();
            let ui = self.imgui.frame();
            self.scene_manager.render_ui(ctx, &ui, style, &self.asset_database);
            let (factory, _, encoder, _, render_target) = graphics::gfx_objects(ctx);
            let draw_data = ui.render();
            self.renderer.render(
                &mut *factory,
                encoder,
                &mut RenderTargetView::new(render_target.clone()),
                draw_data,
            ).unwrap();
        }
        while let Some(command) = self.draw_command_manager.pop() {
            // Wish there was a better way of doing this. Currently, graphics::draw takes a reference to a Sized drawable trait, so dyn Drawable won't work.
            match command {
                DrawCommand::SpriteBatch((batch, params)) => { graphics::draw(ctx, &batch, params)?; }
                DrawCommand::Mesh((mesh, params)) => { graphics::draw(ctx, &mesh, params)?; }
                DrawCommand::Text((text, params)) => { graphics::draw(ctx, &text, params)?; }
                DrawCommand::Canvas((canvas, params)) => { graphics::draw(ctx, &canvas, params)?; }
                DrawCommand::Image((image, params)) => { graphics::draw(ctx, &image, params)?; }
            }
        }
        graphics::present(ctx)?;
        Ok(())
    }

    fn mouse_button_down_event(&mut self, _ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        match button {
            MouseButton::Left => self.mouse_state.pressed.0 = true,
            MouseButton::Right => self.mouse_state.pressed.1 = true,
            MouseButton::Middle => self.mouse_state.pressed.2 = true,
            _ => ()
        }
        self.events.push(Event::MouseButton(MouseButtonArgs::new(button, x, y, true)));
    }

    fn mouse_button_up_event(&mut self, _ctx: &mut Context, button: MouseButton, x: f32, y: f32) {
        match button {
            MouseButton::Left => self.mouse_state.pressed.0 = false,
            MouseButton::Right => self.mouse_state.pressed.1 = false,
            MouseButton::Middle => self.mouse_state.pressed.2 = false,
            _ => ()
        }
        self.events.push(Event::MouseButton(MouseButtonArgs::new(button, x, y, false)));
    }

    fn mouse_motion_event(&mut self, _ctx: &mut Context, x: f32, y: f32, dx: f32, dy: f32) {
        self.mouse_state.pos = (x as i32, y as i32);
        self.events.push(Event::MouseMotion(MouseMotionArgs::new(x, y, dx, dy)));
    }

    fn mouse_enter_or_leave(&mut self, _ctx: &mut Context, entered: bool) {
        self.events.push(Event::MouseEntry(entered));
    }

    fn mouse_wheel_event(&mut self, _ctx: &mut Context, x: f32, y: f32) {
        self.mouse_state.wheel += y;
        self.mouse_state.wheel_h += x;
        self.events.push(Event::MouseWheel(MouseWheelArgs::new(x, y)));
    }

    fn key_down_event(&mut self, _ctx: &mut Context, key_code: KeyCode, key_mods: KeyMods, repeat: bool) {
        self.imgui.io_mut().key_shift = key_mods.contains(KeyMods::SHIFT);
        self.imgui.io_mut().key_ctrl = key_mods.contains(KeyMods::CTRL);
        self.imgui.io_mut().key_alt = key_mods.contains(KeyMods::ALT);
        self.imgui.io_mut().keys_down[key_code as usize] = true;
        self.events.push(Event::KeyEvent(KeyEventArgs::new(key_code, key_mods, repeat, true)));
    }

    fn key_up_event(&mut self, _ctx: &mut Context, key_code: KeyCode, key_mods: KeyMods) {
        if key_mods.contains(KeyMods::SHIFT) {
            self.imgui.io_mut().key_shift = false;
        }
        if key_mods.contains(KeyMods::CTRL) {
            self.imgui.io_mut().key_ctrl = false;
        }
        if key_mods.contains(KeyMods::ALT) {
            self.imgui.io_mut().key_alt = false;
        }
        self.imgui.io_mut().keys_down[key_code as usize] = false;
        self.events.push(Event::KeyEvent(KeyEventArgs::new(key_code, key_mods, false, true)));
    }

    fn text_input_event(&mut self, _ctx: &mut Context, character: char) {
        self.imgui.io_mut().add_input_character(character);
        self.events.push(Event::TextInput(character));
    }

    fn gamepad_button_down_event(&mut self, _ctx: &mut Context, btn: Button, id: GamepadId) {
        self.events.push(Event::GamepadButton(GamepadButtonArgs::new(btn, id, true)));
    }

    fn gamepad_button_up_event(&mut self, _ctx: &mut Context, btn: Button, id: GamepadId) {
        self.events.push(Event::GamepadButton(GamepadButtonArgs::new(btn, id, false)));
    }

    fn gamepad_axis_event(&mut self, _ctx: &mut Context, axis: Axis, value: f32, id: GamepadId) {
        self.events.push(Event::GamepadAxis(GamepadAxisArgs::new(axis, id, value)));
    }

    fn focus_event(&mut self, _ctx: &mut Context, gained: bool) {
        self.events.push(Event::Focus(gained));
    }

    fn quit_event(&mut self, _ctx: &mut Context) -> bool {
        self.events.push(Event::Quit);
        false
    }

    fn resize_event(&mut self, _ctx: &mut Context, width: f32, height: f32) {
        self.events.push(Event::Resize(ResizeArgs::new(width, height)));
    }
}