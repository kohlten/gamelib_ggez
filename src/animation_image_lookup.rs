use crate::{SpriteSheet, DrawCommandManager, DrawCommand};
use std::collections::HashMap;
use ggez::graphics::{Image, DrawParam};
use std::rc::Rc;

pub trait AnimationLookup {
    /// Adds a frame to be drawn for this frame
    fn add_frame(&mut self, idx: usize);
    fn draw(&mut self, param: DrawParam, draw_command_manager: &mut DrawCommandManager);
    fn len(&self) -> usize;
}

pub struct AnimationImageLookup {
    animation: Vec<Image>,
    to_draw: Vec<Image>
}

impl AnimationImageLookup {
    pub fn new(animation: Vec<Image>) -> Self {
        Self { animation, to_draw: Vec::new() }
    }
}

impl AnimationLookup for AnimationImageLookup {
    fn add_frame(&mut self, idx: usize) {
        self.to_draw.push(self.animation[idx].clone());
    }

    fn draw(&mut self, param: DrawParam, draw_command_manager: &mut DrawCommandManager) {
        for image in self.to_draw.iter() {
            draw_command_manager.push(DrawCommand::Image((image.clone(), param)));
        }
        self.to_draw.clear();
    }

    fn len(&self) -> usize {
        self.animation.len()
    }
}

/// Using a SpriteSheet for all of the animations. The SpriteSheet has to have all of the images in it for this to work.
pub struct AnimationSpriteSheetLookup {
    sprite_sheet: SpriteSheet
}

impl AnimationSpriteSheetLookup {
    pub fn new(sprite_sheet: SpriteSheet) -> Self {
        Self {sprite_sheet}
    }
}

impl AnimationLookup for AnimationSpriteSheetLookup {
    fn add_frame(&mut self, idx: usize) {
        self.sprite_sheet.add_draw_section(idx, DrawParam::default());
    }

    fn draw(&mut self, param: DrawParam, draw_command_manager: &mut DrawCommandManager) {
        self.sprite_sheet.draw(draw_command_manager, param);
    }

    fn len(&self) -> usize {
        self.sprite_sheet.len()
    }
}

