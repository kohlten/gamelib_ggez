use ggez::graphics::spritebatch::SpriteBatch;
use ggez::graphics::{Mesh, Text, Canvas, Image, DrawParam};
use event_manager::EventManager;

pub enum DrawCommand {
    SpriteBatch((SpriteBatch, DrawParam)),
    Mesh((Mesh, DrawParam)),
    Text((Text, DrawParam)),
    Canvas((Canvas, DrawParam)),
    Image((Image, DrawParam))
}

pub type DrawCommandManager = EventManager<DrawCommand>;