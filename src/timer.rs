use std::time::{Duration, Instant};
use std::ops::Add;

pub struct Timer {
    start: Instant,
    target: Instant,
    duration: Duration,
    started: bool,
}

impl Timer {
    pub fn new(duration: Duration) -> Timer {
        Timer{ start: Instant::now(), target: Instant::now(), duration, started: false }
    }

    pub fn set_target_time(&mut self, duration: Duration) {
        self.duration = duration;
    }

    pub fn start(&mut self) {
        self.start = Instant::now();
        self.target = Instant::now().add(self.duration);
        self.started = true;
    }

    pub fn stop(&mut self) {
        self.started = false;
    }

    pub fn reset(&mut self) {
        self.stop();
        self.start();
    }

    pub fn get_elapsed_time(&self) -> Duration {
        self.start.elapsed()
    }

    pub fn is_completed(&self) -> bool {
        Instant::now() > self.target
    }

    pub fn is_running(&self) -> bool {
        return !self.is_completed() && self.started;
    }
}

#[cfg(test)]
mod tests {
    use crate::timer::Timer;
    use std::time::{Duration, Instant};

    #[test]
    fn timer_basic() {
        let mut timer = Timer::new(Duration::from_millis(500));
        timer.start();
        let start = Instant::now();
        while !timer.is_completed() {}
        assert_eq!(start.elapsed().as_millis(), 500);
    }
}