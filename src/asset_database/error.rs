use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Asset {name:?} already exists")]
    AssetAlreadyExists {
        name: String
    },
    #[error("Asset {name:?} doesn't exist")]
    AssetDoesntExist {
        name: String
    }
}