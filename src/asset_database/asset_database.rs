use std::collections::HashMap;
use crate::asset_database::{Asset, Error};

/// Database of assets. Allows loading and then retrieval later, so we don't have to load assets in every scene.
pub struct AssetDatabase {
    data: HashMap<String, Asset>
}

impl AssetDatabase {
    /// Creates a new database
    pub(crate) fn new() -> AssetDatabase {
        AssetDatabase {
            data: HashMap::new(),
        }
    }

    /// Adds an asset to the database under the name. Will return AssetAlreadyExists if the asset is already in the database.
    pub fn push_asset(&mut self, name: &str, value: Asset) -> Result<(), Error> {
        if self.data.contains_key(name) {
            return Err(Error::AssetAlreadyExists { name: name.to_string() });
        }
        self.data.insert(name.to_string(), value);
        Ok(())
    }

    /// Removes asset under name. If the asset doesn't exist, returns AssetDoesntExist.
    pub fn remove_asset(&mut self, name: &str) -> Result<(), Error> {
        if !self.data.contains_key(name) {
            return Err(Error::AssetDoesntExist { name: name.to_string() });
        }
        self.data.remove(name);
        Ok(())
    }

    /// Returns the asset under name. If the asset doesn't exist, returns AssetDoesntExist.
    pub fn get_asset(&self, name: &str) -> Result<&Asset, Error> {
        if !self.data.contains_key(name) {
            return Err(Error::AssetDoesntExist { name: name.to_string() });
        }
        Ok(self.data.get(name).unwrap())
    }
}