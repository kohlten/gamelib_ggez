use std::rc::Rc;
use std::cell::RefCell;
use std::fs::File;
use std::io::Read;
use ggez::{Context, graphics::spritebatch::SpriteBatch, graphics::{DrawParam, Image, Color, Rect}};
use glam::Vec2;
use anyhow::Result;
use crate::draw_command::{DrawCommand, DrawCommandManager};
use crate::image_text::glyph_table::GlyphTable;

// @TODO Allow for multiple colors for each char
#[derive(Clone)]
/// Text value to display
pub struct ImageTextValue {
    value: String, // String to use
    pos: Vec2, // Position on the display
    color: Color, // Color of the whole text
    font_size: i32 // Size of this font
}

impl ImageTextValue {
    /// Creates a new ImageTextValue
    pub fn new(value: &str, pos: Vec2, color: Color, font_size: i32) -> Self {
        Self { value: value.to_string(), pos, color, font_size }
    }

    /// Returns the current string to display
    pub fn get_value(&self) -> &str {
        return &self.value
    }

    /// Sets the current string to display
    pub fn set_value(&mut self, value: &str) {
        self.value = value.to_string();
    }

    /// Returns the position to display the text on
    pub fn get_pos(&self) -> Vec2 {
        self.pos
    }

    /// Sets the position to display the text on
    pub fn set_pos(&mut self, pos: Vec2) {
        self.pos = pos;
    }

    /// Returns the color to display the text as
    pub fn get_color(&self) -> Color {
        self.color
    }

    /// Sets the color to display as
    pub fn set_color(&mut self, color: Color) {
        self.color = color
    }

    /// Returns the current font size
    pub fn get_font_size(&self) -> i32 {
        self.font_size
    }

    /// Sets the font size
    pub fn set_font_size(&mut self, font_size: i32) {
        self.font_size = font_size
    }
}

/// ImageText allows for using a single image to display text. This is done through a SpriteBatch, so it is efficient when drawing.
pub struct ImageText {
    batch: SpriteBatch, // Image contained in a SpriteBatch
    image_size: Vec2, // Size of the underlying image
    table: GlyphTable, // GlyphTable to get information about each glyph
    values: Rc<RefCell<Vec<ImageTextValue>>>, // List of values to display
}

impl ImageText {
    /// Create a new ImageText from a GlyphTable. Returns an error if it could not open the Image from GlyphTable.
    pub fn new(table: GlyphTable, ctx: &mut Context) -> Result<Self> {
        let mut data = Vec::new();
        File::open(table.get_image())?.read_to_end(&mut data)?;
        let image = Image::from_bytes (ctx, &data)?;
        let image_size = Vec2::new(image.width() as f32, image.height() as f32);
        let batch = SpriteBatch::new(image);
        Ok(Self { batch, image_size, table, values: Rc::new(RefCell::new(Vec::new())) })
    }

    /// Returns a value located at the index
    pub fn get_value(&self, idx: usize) -> Option<ImageTextValue> {
        if idx >= self.values.borrow().len() {
            return None;
        }
        Some(self.values.borrow()[idx].clone())
    }

    /// Sets a value located at index
    pub fn set_value(&mut self, idx: usize, value: ImageTextValue) {
        if idx >= self.values.borrow().len() {
            return;
        }
        self.values.borrow_mut()[idx] = value;
    }

    /// Adds a new value to this ImageText
    pub fn add_value(&mut self, value: ImageTextValue) -> usize {
        self.values.borrow_mut().push(value);
        self.values.borrow_mut().len() - 1
    }

    /// Removes a value from this ImageText
    pub fn remove_value(&mut self, idx: usize) {
        self.values.borrow_mut().remove(idx);
    }

    /// Updates all of the values and turns them into DrawParams and then adds the SpriteBatch to the DrawCommandManager.
    pub fn draw(&mut self, draw_command_manager: &mut DrawCommandManager) {
        // @TODO "batch.clear()" might not be the best way of doing it.
        // @TODO The DrawParam for each character is recreated for each draw call.
        // @TODO It would be more preferable to do it on an update of the value, but we don't have a remove for SpriteBatch.
        // @TODO One way to possibly improve this would be to take the DrawParams and store them so we don't redo the calculations, but it still doesn't fix the SpriteBatch problem.
        self.batch.clear();
        let values = self.values.clone();
        for value in values.borrow_mut().iter() {
            self.update_value(value);
        }
        draw_command_manager.push(DrawCommand::SpriteBatch((self.batch.clone(), DrawParam::default())));
    }

    /// Calculates the DrawParams for a value
    fn update_value(&mut self, value: &ImageTextValue) {
        let mut pos = value.get_pos();
        let scale = self.calculate_scale(value);
        for ch in value.get_value().chars() {
            if ch == '\n' {
                pos.x = value.get_pos().x;
                pos.y += self.table.get_size() * scale;
            } else if ch == ' '{
                pos.x += (self.table.get_size() / 2.0) * scale; // @TODO Is this right? The Y size could be much greater than the X size of each letter.
            } else {
                let node = match self.table.get_node(ch) {
                    None => {
                        println!("ERROR: Failed to get char {} with image_text {}", ch, self.table.get_image());
                        continue;
                    }
                    Some(node) => { node }
                };
                let mut param = DrawParam::new();
                param = param.color(value.get_color());
                let dest = pos.to_array();
                param = param.dest(dest);
                let node_pos = node.get_pos();
                let mut h = self.table.get_size() / self.image_size[1];
                if h > 1.0 {
                    h = 1.0;
                }
                param = param.scale([scale as f32, scale as f32]);
                let rect = Rect::new(node_pos[0] / self.image_size[0],node_pos[1] / self.image_size[1],  node.get_size() / self.image_size[0], h);
                param = param.src(rect);
                self.batch.add(param);
                pos.x += node.get_size() * scale;
            }
        }
    }

    /// Calculate the scalar value based on the base font size and the wanted size
    fn calculate_scale(&self, value: &ImageTextValue) -> f32 {
        value.get_font_size() as f32 / self.table.get_size()
    }
}