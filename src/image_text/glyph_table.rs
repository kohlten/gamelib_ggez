use std::collections::HashMap;
use std::fs::File;
use std::io::{Read, Write};
use anyhow::Result;
use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct GlyphNode {
    size: f32, // the x size of this char
    pos: [f32; 2], // position on the image
}

impl GlyphNode {
    /// Creates a new GlyphNode
    pub fn new(size: f32, pos: [f32; 2]) -> Self {
        Self {
            size,
            pos,
        }
    }

    /// Returns the z size of this char
    pub fn get_size(&self) -> f32 {
        self.size
    }

    /// Returns the position on the underlying image
    pub fn get_pos(&self) -> [f32; 2] {
        self.pos
    }
}

#[derive(Serialize, Deserialize)]
pub struct GlyphTable {
    image_name: String, // Image with the font on it
    size: f32, // y size of this font
    table: HashMap<String, GlyphNode>,
}

impl GlyphTable {
    /// Creates a new GlyphTable
    pub fn new(table: HashMap<String, GlyphNode>, image_name: &str, size: f32) -> Self {
        Self { table, image_name: image_name.to_string(), size }
    }

    /// Creates a new GlyphTable from a toml config (See examples/image_text.rs, examples/z.png, examples/z.toml for more information)
    pub fn from_config(path: &str) -> Result<Self> {
        let mut data = Vec::new();
        File::open(path)?.read_to_end(&mut data)?;
        match toml::from_slice(&data) {
            Ok(table) => { Ok(table) }
            Err(err) => { Err(anyhow::anyhow!(err)) }
        }
    }

    /// Creates a config from this GlyphTable
    pub fn to_config(&self, path: &str) -> Result<()> {
        let serialized = toml::to_string(&self)?;
        match File::create(path)?.write_all(serialized.as_bytes()) {
            Ok(_) => Ok(()),
            Err(err) => { Err(anyhow::anyhow!(err)) }
        }
    }

    /// Gets a char from the table
    pub fn get_node(&self, id: char) -> Option<&GlyphNode> {
        self.table.get(&id.to_string())
    }

    /// Removes a node from the table
    pub fn remove_node(&mut self, id: char) {
        self.table.remove(&id.to_string());
    }

    /// Returns the path to the image
    pub fn get_image(&self) -> &str {
        &self.image_name
    }

    /// Returns the y size of this table
    pub fn get_size(&self) -> f32 {
        self.size
    }
}